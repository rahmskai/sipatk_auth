<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    /* add or update data */
    protected $fillable = [
        'name',
    ];

    public function people()
    {
        return $this->hasMany(User::class);
    }
}
