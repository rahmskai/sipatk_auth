<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Http\Requests;
use App\BinInventory;
use App\Inventory;
use App\InventoryCard;
use App\InventoryRequest;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $inventories = Inventory::where('isShown', 1)
                        ->orderBy('name', 'asc')
                        ->with(['cards' => function($q) {
                            $q->orderBy('created_at', 'desc');
                        }])->get();
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
        // return $requests;

        // return $inventories;
        return view('inventory.index', compact('inventories', 'count_requests'));
    	// return $cards;
    }

    public function create()
    {
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
    	return view('inventory.create', compact('count_requests'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:inventories,name',
            'min_stock' => 'required|integer',
            'category' => 'required',
        ]);

        if ($validator->fails()) {
            session()->flash('flash_message', 'Brang telah terdaftar.');
            return redirect()->route('inventory-create')
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $input = $request->all();
            $input['status'] = 0;
            $input['isShown'] = 1;
            
            $inventory = Inventory::create($input);

            // Hack to get the last inserted inventory
            $newInventory = Inventory::get()->last();

            $newInventory->cards()->create([
                'bill_num' => 0,
                'from' => '-',
                'to' => '-',
                'in' => 0,
                'out' => 0,
                'stock' => 0,
                'isShown' => 0,
            ]);

            session()->flash('flash_message', 'Terima kasih, '.$newInventory->name.' telah dibuat.');
            return redirect()->route('inventory-index');
        }
    }

    public function update($id)
    {
        $inventory = Inventory::findOrFail($id);
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
        return view('inventory.update', compact('inventory', 'count_requests'));
        // return $inventory;
    }

    public function storeUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'min_stock' => 'required|integer',
            'category' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('barang/'.$id.'/ubah')
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $inventory = Inventory::find($id);
            // return $inventory;
            new_binInventory($inventory);

            $input = $request->all();
            Inventory::where('id', $id)->update([
                    'name' => $input['name'],
                    'min_stock' => $input['min_stock'],
                    'category' => $input['category'],
            ]);
            // return $inventory;
            session()->flash('flash_message', $inventory->name.' berhasil diubah.');
            return redirect()->route('inventory-index');
        }
    }

    public function delete(Request $request, $id)
    {
        $inventory = Inventory::find($id);
        new_binInventory($inventory);

        Inventory::where('id', $id)->update([
                'isShown' => 0,
            ]);
        // return $new_bin;
        session()->flash('flash_message', $inventory->name.' berhasil dihapus.');
        return redirect()->route('inventory-index');
    }
}
