<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\InventoryRequest;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$count_requests = InventoryRequest::where('status', 0)->get()->count();
    	// return session()->all();
    	return view('dashboard', compact('count_requests'));
    }
}
