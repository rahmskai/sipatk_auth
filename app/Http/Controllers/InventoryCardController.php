<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DateTime;

use App\Http\Requests;
use App\Inventory;
use App\InventoryCard;
use App\InventoryRequest;

class InventoryCardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function card($id)
    {
        $inventory = Inventory::findOrFail($id);
        $card = $inventory->cards()->where('isShown', 1)->orderBy('created_at', 'desc')->get();
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
        return view('inventory-card.index', compact('card', 'inventory', 'count_requests'));
        // return $card;
        // return $inventory;
    }

    public function create($id)
    {
    	$inventory = Inventory::findOrFail($id);
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
    	return view('inventory-card.create', compact('inventory', 'count_requests'));
        // return $inventory;
    }

    public function store(Request $request, $id)
    {
        // return "lah";
    	$input = $request->all();
        $validator = Validator::make($request->all(), [
            'transaction_date' => 'required|date',
            'bill_num' => 'required',
            'in' => 'integer',
            'out' => 'integer',
        ]);

        // return $input;
        if ($validator->fails()) {
            // return "cek1";
            // return $request->all();
            return redirect()->route('card-create', $id)
                    ->withErrors($validator)
                    ->withInput();
        } else {
            // return "cek2";
            if($input['in'] == "" && $input['out'] == "") {
                session()->flash('flash_message', 'Mohon masukkan data transaksi masuk/keluar.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else if ($input['in'] != "" && $input['from'] == "") {
                session()->flash('flash_message', 'Jumlah masuk hanya dapat diisi jika asal barang terisi.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else if ($input['out'] != "" && $input['to'] == "") {
                session()->flash('flash_message', 'Jumlah keluar hanya dapat diisi jika tujuan barang terisi.');
                return redirect()->route('card-create', $id)
                    ->withInput();
            } else {
                $inventory = Inventory::find($id);

                $input['inventory_id'] = $id;
                $t_date = DateTime::createFromFormat('d-m-Y', $input['transaction_date'])->format('Y-m-d');
                $input['transaction_date'] = $t_date;
                if ($input['from'] == "")
                    $input['from'] = "-";
                if ($input['to'] == "")
                    $input['to'] = "-";
                if ($input['in'] == "")
                    $input['in'] = 0;
                if ($input['out'] == "")
                    $input['out'] = 0;
                $input['isShown'] = 1;
                $input['stock'] = get_stock($inventory->cards()->orderBy('created_at', 'desc')->first(), $input['in'], $input['out']);
                // return $input;
                
                InventoryCard::create($input);
                // update_stocks($inventory->cards()->orderBy('created_at', 'asc')->get());
                // return $inventory->cards()->get();
                
                // $new_card = InventoryCard::where('inventory_id', $id)->orderBy('transaction_date', 'desc')->first();
                // return [$new_card->stock, $new_card->status, $inventory->min_stock];

                set_inventoryStatus($inventory->cards()->orderBy('created_at', 'desc')->first(), $inventory);
                // return $inventory;

                session()->flash('flash_message', 'Terima kasih, transaksi baru pada tanggal '.$input['transaction_date'].' berhasil dimasukkan.');
            	return redirect()->route('card-detail', $id);   
            }
        }
    }

    public function export($id)
    {
        $cards = InventoryCard::with('inventory')->where('inventory_id', 1)->get();
        $excel = \App::make('excel');

        \Excel::create('Laporan Kartu Barang', function($excel) use($cards) {
            $excel->sheet('Sheet 1', function($sheet) use ($cards) {
                $sheet->setOrientation('landscape');
                // $sheet->with($cards);
                $sheet->appendRow([
                    'No Bon Gudang',
                    'Dari',
                    'Kepada',
                    'Masuk',
                    'Keluar',
                    'Stok',
                    'Dibuat Pada',
                ]);

                foreach ($cards as $card) {
                    $sheet->appendRow([
                        $card->bill_num,
                        $card->from,
                        $card->to,
                        $card->in,
                        $card->out,
                        $card->stock,
                        $card->created_at,
                    ]);
                }
            });
        })->export('xls');
    }
}
