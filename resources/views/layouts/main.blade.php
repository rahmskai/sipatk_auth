<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('page-title') SIPATK</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 -->
    <!--CSS PLUGINS-->
    <link rel="stylesheet" href="{{ asset('assets/css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('assets/css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="stylesheet" href="{{ asset('assets/css/prism.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    
    @yield('styles')

    <link rel="stylesheet" href="{{ asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/jvectormap/jquery-jvectormap.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="icon" href="{{ asset('favicon.ico') }}">

</head>
<body>
    <!-- <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div> -->

    @yield('navbar')
	@yield('content')
    @yield('footer')

    <!-- jQuery Library -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script> 
    <!--materialize js-->
    <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
    <!--prism-->
    <script type="text/javascript" src="{{ asset('assets/js/prism.js') }}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/sparkline/sparkline-script.js') }}"></script>
    
    <!--jvectormap-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/jvectormap/vectormap-script.js') }}"></script>
    
	@yield('scripts')

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
    

    <script src="{{ asset('assets/js/app.js') }}"></script>  
</body>
</html>

