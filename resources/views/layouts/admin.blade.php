@extends('layouts.main')

@section('navbar')
<header id="header" class="page-topbar">
    <div class="navbar-fixed">
        <nav class="grey darken-3">
            <div class="nav-wrapper">
                <a href="/" class="hide-on-med-and-down" style="margin-left: 260px;">Sistem Informasi Pengelolaan ATK</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
@endsection

@section('sidebar')
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation collapsible collapsible-accordion">
    	<li class="bold"><a href="/" class="waves-effect waves-cyan"><img src="{{ asset('assets/images/logobi.png') }}"></a>
        </li>
        <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s12 m12 l12">
                    <ul id="profile-dropdown" class="dropdown-content">
                        </li>
                        <li><a href="#"><i class="mdi-communication-live-help"></i>Bantuan</a>
                        </li>
                        <li class="divider"></li>
                        </li>
                        <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                        </li>
                    </ul>
                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">John Doe<i class="mdi-navigation-arrow-drop-down right"></i></a>
                    <p class="user-roal">Administrator</p>
                </div>
            </div>
        </li>
        <li><a href="/" class="waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Dashboard</a>
        </li>
        <li><a href="{{ route('inventory-index') }}" class="waves-effect waves-cyan"><i class="mdi-device-now-widgets"></i> Barang</a>
        </li>
        <li><a href="{{ route('request-index') }}" class="waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> Permintaan <span class="new badge">@yield('new_request')</span></a>
        </li>
        <li><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-shopping-cart"></i> Permintaan</a>
            <div class="collapsible-body">
                <ul>
                   	<li><a href="{{ route('request-create') }}" class="waves-effect waves-cyan"><i class="mdi-content-add-box"></i> Buat Permintaan</a>
       				</li>
                    <li><a href="{{ route('request-index-user', 'skv') }}" class="waves-effect waves-cyan"><i class="mdi-toggle-check-box"></i> Riwayat Permintaan</a>
        			</li>
                </ul>
            </div>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="mdi-navigation-menu" ></i></a>
</aside>
@endsection

@section('content')
<div id="main">
    <div class="wrapper">
        @yield('sidebar')
        <section id="content">
            <!--BREADCRUMBS-->
            <div id="breadcrumbs-wrapper" class="grey lighten-3">
                <div class="container">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <h5 class="breadcrumbs-title">@yield('title')</h5>
                            <ol class="breadcrumb">
                                @yield('root')
                                @yield('previous')
                                <li class="active">@yield('here')</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!--MAIN CONTENT-->
            <div class="container">
                <div class="section">
                    @yield('admin-content')
                </div>
            </div>
    </div>
</div>
</section>
@endsection

@section('footer')
<footer class="page-footer grey darken-3">
    <div class="footer-copyright grey darken-3">
        <div class="container">
           © 2016 <a class="grey-text text-lighten-4" href="http://www.bi.go.id" target="_blank">Bank Indonesia</a>
        </div>
    </div>
</footer>
@endsection

@section('scripts')
@endsection