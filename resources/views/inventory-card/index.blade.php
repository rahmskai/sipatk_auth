@extends('layouts.admin')
@section('page-title', 'Kartu Barang | ')
@section('title', 'Kartu Barang')
@section('root', '<li><a href="/">Dashboard</a></li>')
@section('previous')
  <li><a href="{{ route('inventory-index') }}">Daftar Barang</a></li>
@endsection
@section('here', 'Kartu Barang')
@section('new_request', $count_requests)
@section('styles')
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('admin-content')
<div class="container col s12 m8 l9">
  <div class="right">
    <a href="{{ route('card-create', $inventory->id) }}" class="btn waves-effect waves-light indigo darken-4"><i class="mdi-content-add"></i></a>
    <a href="{{ route('card-export', $inventory->id) }}" class="btn waves-effect waves-light indigo darken-4"><i class="mdi-action-print"></i></a>
  </div>
  <div id="table-datatables">
    <h4 class="header">Kartu Barang {{ $inventory->name }}</h4>
    <div class="row">
      <div class="col s12 m12 l12">
        @if (session()->has('flash_message'))
            <div id="card-alert" class="card blue darken-1">
              <div class="card-content white-text darken-1">
                  <p class="single-alert">{{ session('flash_message') }}</p>
              </div>
            </div>
            <br>
        @endif
        <table id="data-table-simple" class="responsive-table display centered" cellspacing="0">
          <thead>
              <tr>
                  <th>No. Bon</th>
                  <th>Tanggal Transaksi</th>
                  <th>Dari</th>
                  <th>Kepada</th>
                  <th>Masuk</th>
                  <th>Keluar</th>
                  <th>Stok</th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>No. Bon</th>
                  <th>Tanggal Transaksi</th>
                  <th>Dari</th>
                  <th>Kepada</th>
                  <th>Masuk</th>
                  <th>Keluar</th>
                  <th>Stok</th>
              </tr>
          </tfoot>
          <tbody>
            @foreach ($card as $a_card)
              <tr>
                <td>{{ $a_card->bill_num }}</td>
                <td>{{ $a_card->transaction_date }}</td>
                <td>{{ $a_card->from ?: "-"}}</td>
                <td>{{ $a_card->to ?: "-"}}</td>
                <td>{{ $a_card->in }}</td>
                <td>{{ $a_card->out }}</td>
                <td>{{ $a_card->stock }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
@endsection