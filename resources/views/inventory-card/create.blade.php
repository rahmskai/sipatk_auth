@extends('layouts.admin')
@section('page-title', 'Transaksi Baru | ')
@section('title', 'Transaksi Baru')
@section('root', '<li><a href="/">Dashboard</a></li>')
@section('previous')
  <li><a href="{{ route('inventory-index') }}">Daftar Barang</a></li>
  <li><a href="{{ route('card-detail', $inventory->id) }}">Kartu Barang</a></li>
@endsection
@section('here', 'Transaksi Baru')
@section('new_request', $count_requests)
@section('admin-content')
<div class="col s12 m8 l9">
	<!-- <div class="card"> -->
	<div id="jqueryvalidation" class="section">
		<div class="container" id="card-create">
			<h4>Transaksi Baru untuk {{ $inventory->name }}</h4>
			<br>
            <div id="card-alert" class="card blue darken-1">
				<div class="card-content white-text darken-1">
					<p>Area dengan (*) wajib diisi.</p>	
					@if (session()->has('flash_message'))
						<p class="single-alert">{{ session('flash_message') }}</p>
				    @endif
				</div>
            </div>
            <br>
			<form id="formValidate" class="row formValidate" method="post" action="{{ route('card-create', $inventory->id) }}">
				{{ csrf_field() }}
				<div class="input-field col s12 m6 l6 validate">
					<label for="transaction_date">Waktu Transaksi*</label>
					<input type="date" name="transaction_date" class="datepicker" data-error=".errorTime">
					<div class="errorTime"></div>
				</div>
				<div class="input-field col s12 m6 l6 validate">
					<label for="bill_num">No. Bon Barang*</label>
					<input type="text" name="bill_num" data-error=".errorBill">
					<div class="errorBill"></div>
				</div>
				<div class="input-field col s12 m6 l6">
					<label for="from">Dari</label>
					<input id="from" type="text" name="from" data-error=".errorFrom">
					<div class="errorFrom"></div>
				</div>
				<div class="input-field col s12 m6 l6">
					<label for="to">Kepada</label>
					<input id="to" type="text" name="to" data-error=".errorTo">
					<div class="errorTo"></div>
				</div>
				<div class="input-field col s12 m6 l6">
					<label for="in">Jumlah Masuk</label>
					<input type="text" name="in" data-error=".errorIn">
					<div class="errorIn"></div>
				</div>
				<div class="input-field col s12 m6 l6">
					<label for="out">Jumlah Keluar</label>
					<input type="text" name="out" data-error=".errorOut">
					<div class="errorOut"></div>
				</div>
				<div class="col s12">
					<br>
					<button class="btn waves-effect waves-light indigo darken-4 create-card">Simpan</button>
				</div>
			</form>
		</div>
	</div>
	<!-- </div> -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
	$('.datepicker').pickadate({
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15, // Creates a dropdown of 15 years to control year
	    format: 'dd-mm-yyyy' 
	});
    $("#formValidate").validate({
        rules: {
        	transaction_date: {
        		required: true,
        	},
            bill_num: {
                required: true,
            },
            in: {
            	digits: true,
            	required: {
	            		depends: function(element){
						return $("#from").val().length > 0;
					},
		        },
            },
            out: {
            	digits: true,
            	required: {
	            		depends: function(element){
						return $("#to").val().length > 0;
					},
		        },
            }
		},
        //For custom messages
        messages: {
        	transaction_date: {
        		required: "Tanggal transaksi wajib diisi",
        	},
            bill_num:{
                required: "Nomor bon gudang harus diisi",
            },
            in: {
            	digits: "Jumlah masuk harus berupa angka",
            	required: "Asal barang harus diisi",
            },
            out: {
            	digits: "Jumlah keluar harus berupa angka",
            	required: "Tujuan barang harus diisi",
            }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection