<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DivisionsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(InventoriesSeeder::class);
        $this->call(InventoryCardsSeeder::class);
        $this->call(RequestsSeeder::class);
        $this->call(RequestedInventoriesSeeder::class);
        $this->call(UnitQuantitySeeder::class);
    }
}
