<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'skv',
            'password' => bcrypt('t3st'),
            'NIP' => '12345',
            'email' => 'rahmskai@gmail.com',
            'name' => 'Rahma Khairunisa Nursalamah',
            'division_id' => 1,
            'isAdmin' => 1,
        ]);

        DB::table('users')->insert([
            'username' => 'annisks',
            'password' => bcrypt('t3st'),
            'NIP' => '98765',
            'email' => 'annisks07@gmail.com',
            'name' => 'Annis Khairunisa Safitri',
            'division_id' => 1,
            'isAdmin' => 0,
        ]);
    }
}
