<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InventoryCardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('inventorycards')->insert([
        // 	'inventory_id' => 1,
        // 	'bill_num' => 550,
        // 	'from' => 'Gramedia',
        // 	'to' => '',
        // 	'in' => 10,
        // 	'out' => 0,
        //     'isShown' => 1,
        //     'created_at' => Carbon::now('Asia/Jakarta'),
        //     'updated_at' => Carbon::now('Asia/Jakarta'),
        // ]);
        // DB::table('inventorycards')->insert([
        //     'inventory_id' => 1,
        //     'bill_num' => 551,
        //     'from' => 'Gramedia',
        //     'to' => '',
        //     'in' => 10,
        //     'out' => 0,
        //     'isShown' => 0,
        //     'created_at' => Carbon::now('Asia/Jakarta'),
        //     'updated_at' => Carbon::now('Asia/Jakarta'),
        // ]);
        // DB::table('inventorycards')->insert([
        //     'inventory_id' => 2,
        //     'bill_num' => 553,
        //     'from' => 'DLP',
        //     'to' => '',
        //     'in' => 10,
        //     'out' => 0,
        //     'isShown' => 0,
        //     'created_at' => Carbon::now('Asia/Jakarta'),
        //     'updated_at' => Carbon::now('Asia/Jakarta'),
        // ]);
        // DB::table('inventorycards')->insert([
        //     'inventory_id' => 3,
        //     'bill_num' => 554,
        //     'from' => 'Toko Buku Gunung Agung',
        //     'to' => '',
        //     'in' => 0,
        //     'out' => 10,
        //     'isShown' => 0,
        //     'created_at' => Carbon::now('Asia/Jakarta'),
        //     'updated_at' => Carbon::now('Asia/Jakarta'),
        // ]);
    }
}
